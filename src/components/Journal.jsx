import React, { useState, useEffect, useRef } from "react";
import { BsCalendar2Check } from "react-icons/bs";

const Journal = () => {
  const currentDate = new Date();
  const calendarRef = useRef(null);
  const [selectedDate, setSelectedDate] = useState(null);
  const [journalEntry, setJournalEntry] = useState("");

  const getDaysInMonth = (year, month) => {
    return new Date(year, month + 1, 0).getDate();
  };

  const generateDates = () => {
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth();
    const daysInMonth = getDaysInMonth(year, month);

    const dates = [];
    for (let i = 1; i <= daysInMonth; i++) {
      dates.push(i);
    }

    return dates;
  };

  const scrollToToday = () => {
    const element = calendarRef.current;
    if (element) {
      const todayElement = element.querySelector(".today");
      if (todayElement) {
        handleDateClick(currentDate.getDate());
        element.scrollTo({
          left:
            todayElement.offsetLeft -
            (element.offsetWidth / 2 - todayElement.offsetWidth / 2),
          behavior: "smooth",
        });
      }
    }
  };

  const handleDateClick = (date) => {
    setSelectedDate(date);

    const element = calendarRef.current;
    if (element) {
      const dateElement = element.querySelector(`[data-date="${date}"]`);
      if (dateElement) {
        dateElement.classList.add("selected");
        dateElement.scrollIntoView({
          behavior: "smooth",
          inline: "center",
        });
      }
    }
  };

  const handleJournalEntryChange = (e) => {
    setJournalEntry(e.target.value);
  };

  useEffect(() => {
    scrollToToday();
  }, []);

  useEffect(() => {
    const handleWheelScroll = (e) => {
      const element = calendarRef.current;
      if (element) {
        e.preventDefault();
        element.scrollTo({
          left: element.scrollLeft + e.deltaY * 3,
          behavior: "smooth",
        });
      }
    };

    const calendarElement = calendarRef.current;
    if (calendarElement) {
      calendarElement.addEventListener("wheel", handleWheelScroll, {
        passive: false,
      });
    }

    return () => {
      if (calendarElement) {
        calendarElement.removeEventListener("wheel", handleWheelScroll);
      }
    };
  }, []);

  return (
    <div className="max-w-lg mx-auto relative overflow-y-scroll">
      <div className="header flex justify-end items-center">
        <div className="flex justify-center">
          <button onClick={scrollToToday} className="focus:outline-none">
            <BsCalendar2Check className="text-green-700 text-2xl" />
          </button>
        </div>
        <h1 className="text-center text-2xl font-bold">
          {currentDate.toLocaleString("default", {
            month: "long",
            year: "numeric",
          })}
        </h1>
      </div>

      <div className="overflow-x-auto" ref={calendarRef}>
        <div className="flex whitespace-nowrap gap-4">
          {generateDates().map((date) => (
            <div
              key={date}
              className={`p-2 text-center border border-gray-300 today ${
                currentDate.getDate() === date
                  ? "bg-blue-500 text-white selected"
                  : ""
              }`}
              onClick={() => handleDateClick(date)}
              data-date={date}
            >
              {date}
            </div>
          ))}
        </div>
      </div>

      {selectedDate && (
        <div className="mt-4">
          <h2 className="text-xl font-bold mb-2">
            {currentDate.toLocaleString("default", { month: "long" })}{" "}
            {selectedDate}
          </h2>
          <textarea
            className="border p-2 w-full"
            rows={4}
            value={journalEntry}
            onChange={handleJournalEntryChange}
            placeholder="Write your journal entry..."
          />
        </div>
      )}
    </div>
  );
};

export default Journal;
