import UserProfile from "./ProfilePage";
import TodoList from "./TodoList";
import Journal from "./Journal";
import Calendar from "./userCalendar/Calendar";
const Dashboard = () => {
  return (
    <>
      <div className="user-dashboard grid grid-cols-12">
        <div className="Profile col-span-2">
          <UserProfile />
        </div>
        <div className="TodoList col-span-6 flex flex-col justify-between h-screen">
          <TodoList className="" />
          <Journal className="flex-grow overflow-y-auto" />
        </div>
        <div className="Calendar col-span-4">
          <Calendar />
        </div>
      </div>

      <style jsx>{`
        .TodoList {
          overflow-y: scroll;
        }
      `}</style>
    </>
  );
};

export default Dashboard;
